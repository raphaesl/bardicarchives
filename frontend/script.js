let statsParagraph = document.getElementById('stats');
let clickParagraph = document.getElementById('clickangle');

const colors = [
  "#FFF56D", // Curious
  "#FFEE00", // Adventurous
  "#FFD700", // Intriguing
  "#FF9933", // Enigmatic
  "#FF6600", // Eerie/Creepy
  "#FF4500", // Foreboding
  "#FF0000", // Violent
  "#C71585", // Ambitious
  "#993399", // Scheming
  "#800080", // Grim/Pessimistic
  "#4682B4", // Sad
  "#6495ED", // Nostalgic
  "#00BFFF", // Serene
  "#00FF7F", // Hopeful
  "#32CD32", // Triumphant
  "#7CFC00", // Hearty
  "#FFFF00", // Daring
  "#FFD700"  // Silly
];

const moods = [
  "Curious",
  "Adventurous",
  "Intriguing",
  "Enigmatic",
  "Eerie/Creepy",
  "Foreboding",
  "Violent",
  "Ambitious",
  "Scheming",
  "Grim/Pessimistic",
  "Sad",
  "Nostalgic",
  "Serene",
  "Hopeful",
  "Triumphant",
  "Hearty",
  "Daring",
  "Silly"
];


let rotation = 0;
let targetAngle = 0;
let isSpinning = false;
let wheelCanvas;

function setup() {
  wheelCanvas = createCanvas(800, 800);
  wheelCanvas.parent('container');
  wheelCanvas.mousePressed(spinWheel);

  angleMode(RADIANS);
}

function draw() {
  background(255);
  translate(width / 2, height / 2);

  //translate targetangle to rotation value
  //check if rotation = targetrotation
  //if: stop spin
  //else: continue spin

  rotate(rotation);




  if(round(rotation % TWO_PI, 2) == round(targetAngle,2)){
    stopSpinning();
  }else{
    rotation += isSpinning ? 0.01 : 0;
    stats.innerHTML = "rotation:" + int(rotation) + " rotation%:" + (rotation % TWO_PI);
  }

  drawWheel();
}

function drawWheel() {
  const numSegments = colors.length;
  const angleIncrement = TWO_PI / numSegments;

  for (let i = 0; i < numSegments; i++) {
    const startAngle = i * angleIncrement;
    const endAngle = startAngle + angleIncrement;

    fill(colors[i]);
    arc(0, 0, width * 0.8, height * 0.8, startAngle, endAngle, PIE);

    push();
    rotate(startAngle + angleIncrement / 2);
    textAlign(CENTER, CENTER);
    fill(255);
    textSize(24);
    text(moods[i], width * 0.25, 0);
    pop();
  }
}

function spinWheel(event) {
  if (!isSpinning) {
    //get click position and calculate click angle
    const clickAngle = atan2(mouseY - height / 2, mouseX - width / 2); //get angle from origo

    //set target angle as global variable
    /* min shit kode
    if (clickAngle < 0) {
      targetAngle = PI + (PI + clickAngle)
    }else{
      targetAngle = clickAngle
    }*/

    if (clickAngle < 0) {
      targetAngle = clickAngle * -1
    }else {
      targetAngle = PI + (PI - clickAngle)
    }

    //targetAngle = (targetAngle % TWO_PI) + (rotation % TWO_PI); //apply offset for previous rotations

    let sectorIncrement = TWO_PI / colors.length;
    for (let i = 0;i < colors.length; i++){
      if(targetAngle >= sectorIncrement * i && targetAngle < sectorIncrement * (i+1)){ //angle is within sector
        console.log("target: " + targetAngle + " lower value: " + sectorIncrement * i + " higher value: " + sectorIncrement * (i+1));
        targetAngle = (sectorIncrement * i) + sectorIncrement/2; 
      }
    }


    clickParagraph.innerHTML = "targetangle: " + targetAngle;


    //start spin
    isSpinning = true;
  }
}

function stopSpinning() {
  isSpinning = false;
}

window.onload = function() {
  wheelCanvas = document.getElementById('wheelCanvas');
  wheelCanvas.style.cursor = 'pointer';
};