package main

import (
	"log"
	"net/http"
	"os"
)

func main() {

	//Webserver---------------------------------------

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	fs := http.FileServer(http.Dir("./frontend"))

	//Make fileserver public
	http.Handle("/frontend/", http.StripPrefix("/frontend", fs))

	log.Fatal(http.ListenAndServe(":"+port, nil))
}
